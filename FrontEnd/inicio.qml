import QtQuick 2.0
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "sharedDefinitions.js" as sharedDefinitions
import "keycloak.js" as keycloak
import "lib/jspdf.min.js"
import "lib/dom-to-image.min.js"

Rectangle {
    color: sharedDefinitions.bgColor
    border.color: "black"
    border.width: 2
    property var papeis;

    Component.onCompleted: {
        papeis = JSON.parse(window.sessionStorage.getItem('papeis'));
        if(!papeis)
            window.sessionStorage.setItem('getPapeis','false');
        else
            window.sessionStorage.removeItem('getPapeis');

       /* domtoimage.toPng(document.getElementById('embed').shadowRoot.children[0]).then(function(dataUrl){
            var imgData = new Image();
            imgData.src=dataUrl;

            var doc = new jsPDF();
            doc.addImage(imgData, 'PNG', 10, 10);
            doc.save('sample-file.pdf');
        });*/
    }

    Row{
        anchors.horizontalCenter: parent.horizontalCenter
        y:50
        spacing: 10

        Rectangle{
            color: "GhostWhite"
            width: 100
            height: 100
            border.color: "black"
            border.width: 2
            radius: 10

            Image {
                anchors.centerIn: parent
                width: parent.width*0.9
                height: parent.height*0.9
                source: "img/report.svg"
            }
            Text {
                text: "Relatórios"
                anchors.top: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                font.pointSize: 12
                font.bold: true
            }
            MouseArea{
                anchors.fill: parent
                onEntered: parent.impl.style.background = 'linear-gradient(LightBlue, GhostWhite)';
                onExited: parent.impl.style.background = 'linear-gradient(GhostWhite, GhostWhite)';
                onClicked: {window.location="/Relatorios"}
            }
        }
        Rectangle{
            color: "GhostWhite"
            width: 100
            height: 100
            border.color: "black"
            border.width: 2
            radius: 10

            Image {
                anchors.centerIn: parent
                width: parent.width*0.9
                height: parent.height*0.9
                source: "img/estante.svg"
            }
            Text {
                text: "Estoque"
                anchors.top: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                font.pointSize: 12
                font.bold: true
            }
            MouseArea{
                anchors.fill: parent
                onEntered: parent.impl.style.background = 'linear-gradient(LightBlue, GhostWhite)';
                onExited: parent.impl.style.background = 'linear-gradient(GhostWhite, GhostWhite)';
                onClicked: {window.location="/Estoque"}
            }
        }
        Rectangle{
            color: "GhostWhite"
            width: 100
            height: 100
            border.color: "black"
            border.width: 2
            radius: 10
            visible: papeis&&papeis.almoxarifado.roles.indexOf("CRUD_Reagentes")>=0

            Image {
                anchors.centerIn: parent
                width: parent.width*0.8
                height: parent.height*0.9
                source: "img/erlenmeyer.png"
            }
            Text {
                text: "Reagentes"
                anchors.top: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                font.pointSize: 12
                font.bold: true
            }
            MouseArea{
                anchors.fill: parent
                onEntered: parent.impl.style.background = 'linear-gradient(LightBlue, GhostWhite)';
                onExited: parent.impl.style.background = 'linear-gradient(GhostWhite, GhostWhite)';
                onClicked: {window.location="/Produtos"}
            }
        }
    }

}
