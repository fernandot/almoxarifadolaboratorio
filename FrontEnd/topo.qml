import QtQuick 2.0
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "sharedDefinitions.js" as sharedDefinitions
import "keycloak.js" as keycloak

Rectangle {
    id:teste
    color: "darkgreen"
    property var kc;
    property bool logado;
    property string usrName;

    Component.onCompleted: {
        impl.style.background += 'linear-gradient(OliveDrab, DarkGreen)';
        kc = keycloak.Keycloak({
                              "realm": "master",
                              "url": sharedDefinitions.kcURL,
                              "ssl-required": "none",
                              "clientId": "almoxarifado",
                              "public-client": true,
                              "use-resource-role-mappings": true,
                              "confidential-port": 0
                            });

        var log = window.sessionStorage.getItem('logado');

        if(window.location.href.length||log)
        {
            //gambi pra chamar o login novamente pra setar as variaveis do keycloak
            var arg = window.location.href.split("#state=");
            if(log||arg.length>1)
            {
                kc.init({ onLoad: 'login-required', redirectUri:window.location.href }).success(kcIniciou).error(function() {
                alert('erro');
                });
            }
        }
    }
    function kcIniciou()
    {
        kc.loadUserInfo().success(function(){});
        kc.loadUserProfile().success(function(){});
        logado=true;
        window.sessionStorage.setItem('logado', 'true');
        window.sessionStorage.setItem('papeis', JSON.stringify(kc.tokenParsed.resource_access));
        window.sessionStorage.setItem('token', JSON.stringify(kc.token));
        window.sessionStorage.setItem('kc', JSON.stringify(kc));
        usrName=kc.tokenParsed.given_name?kc.tokenParsed.given_name:kc.tokenParsed.preferred_username?kc.tokenParsed.preferred_username:"user";
        var getPapeis = window.sessionStorage.getItem('getPapeis');
        if(!kc||(getPapeis&&getPapeis==='false'))
        {
            location.reload();
        }
    }

    Rectangle
    {
        y:0
        anchors.horizontalCenter: parent.horizontalCenter
        width: txtTopo.width
        height: parent.height
        color: "transparent"

        Text {
            id: txtTopo
            text: "Sistema de Almoxarifado"
            anchors.centerIn: parent
            font.pointSize: parent.parent.width>width+100?24:20;
            font.bold: true
            color: "white"
            Component.onCompleted: parent.width=width;
        }
        MouseArea{
            anchors.fill: parent
            onEntered: parent.color= "lightblue";
            onExited: parent.color= "transparent";
            onClicked: window.location=sharedDefinitions.inicialURL;
        }
    }

    Rectangle{
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"
        width: txtLogin.width
        height: 20
        visible: !logado

        Text {
            id:txtLogin
            color:"white"
            text: "Entrar"
        }
        MouseArea{
            anchors.fill: parent
            onEntered: parent.color= "lightblue";
            onExited: parent.color= "transparent";
            onClicked: kc.init({ onLoad: 'login-required', redirectUri:window.location.href }).success(kcIniciou).error(function() {
                alert('erro');
            });
        }
    }
    Rectangle{
        anchors.right: rectLogout.left
        anchors.rightMargin: 5
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"
        width: 200
        height: 20
        visible: logado
        Text {
            id:usrName
            anchors.centerIn: parent
            color:"white"
            text:logado?usrName:"";
            horizontalAlignment: Text.AlignLeft
            onTextChanged: parent.width=width;
        }
    }
    Rectangle{
        id: rectLogout
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.verticalCenter: parent.verticalCenter
        color: "transparent"
        width: txtLogout.width
        height: 20
        visible: logado

        Text {
            id:txtLogout
            color:"white"
            text: "Sair"
        }
        MouseArea{
            anchors.fill: parent
            onEntered: {parent.color= "lightblue"}
            onExited: {parent.color= "transparent"}
            onClicked: {
                window.sessionStorage.removeItem('logado');
                window.sessionStorage.removeItem('papeis');
                kc.logout({redirectUri:sharedDefinitions.inicialURL});
            }
        }
    }

}
