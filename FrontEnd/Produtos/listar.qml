import QtQuick 2.0
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "../sharedDefinitions.js" as sharedDefinitions

Rectangle {
    id:telaListar
    color: sharedDefinitions.bgColor
    border.color: "black"
    border.width: 2
    property var papeis;

    Component.onCompleted: {
        var response = window.sessionStorage.getItem('responseProduto');

        if(response)
        {
            txtResponse.text=response;
            txtResponse.visible=true;
        }
        window.sessionStorage.removeItem('responseProduto');

        papeis = JSON.parse(window.sessionStorage.getItem('papeis'));
        if(!papeis)
            window.sessionStorage.setItem('getPapeis','false');
        else
            window.sessionStorage.removeItem('getPapeis');
    }

    WebSocket {
        id: socket
        url: sharedDefinitions.wsProdutoURL
        active: false
        onTextMessageReceived: {
            var obj = JSON.parse(message);
            for (var i = 0; i < obj.lista.length; i++) {
                listModelNomes.append({"IDProduto":obj.lista[i].IDProduto,
                "Nome":obj.lista[i].Nome});
            }

            socket.active=false;
        }
        onStatusChanged: {if(socket.status===WebSocket.Open){
                var keysList = JSON.stringify({"Acao":"Listar",
                                               "Filtro":lstInput.text,
                                               "token":window.sessionStorage.getItem('token')});
                socket.sendTextMessage(keysList);
            }
        }
    }

    Text {
        text: "Listar Reagentes"
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 24
        font.bold: true
    }

    Row {
        anchors.horizontalCenter: parent.horizontalCenter
        y: 100
        spacing: 5

        Text{
            text: "Nome"
            font.bold: true
        }

        TextInput {
            id:lstInput
            width: 200
        }

        Button {
            text: "Listar"
            onClicked: {
                listModelNomes.clear();
                socket.active = true;
                txtResponse.visible=false;
            }
        }
        Button {
            text: "Novo"
            onClicked: window.location = "/Produtos/CadastroProdutos";
        }
    }

    ListModel {
        id: listModelNomes
    }
    Column{
        anchors.horizontalCenter: parent.horizontalCenter
        y:150
        onHeightChanged: {
            document.body.style.height="90%";
            if(200+height>window.innerHeight) {
                document.body.style.height=150+height+"px";
                telaListar.height=150+height;
            }
        }
        ListView {
            id: listaNomes
            model: listModelNomes
            delegate: Component {
                id: nomeDelegate

                Rectangle {
                    id: nomeDelegateRect
                    width: 180
                    height: 20
                    border.color: "black"

                    MouseArea{
                        anchors.fill: parent
                        onEntered: {parent.color= "lightblue"}
                        onExited: {parent.color= "white"}
                    }

                    Rectangle{
                        color: "white"
                        width: 20
                        height: 20
                        border.color: "black"
                        border.width: 1
                        radius: 2
                        anchors.left: nomeDelegateRect.right

                        Image {
                            anchors.centerIn: parent
                            width: parent.width*0.7
                            height: parent.height*0.7
                            source: "../img/edit.svg"
                        }
                        MouseArea{
                            anchors.fill: parent
                            onEntered: {parent.color= "lightblue"}
                            onExited: {parent.color= "white"}
                            onClicked: {
                                window.sessionStorage.setItem('editarProduto',IDProduto);
                                window.location = "/Produtos/CadastroProdutos";}
                        }
                    }

                    Text {
                        text: Nome;
                    }
                }
            }
            highlight: Rectangle {
                width: 180
                height: 40
                color: "lightsteelblue"
                radius: 5
            }
            focus: true
        }
    }

    Text {
        id:txtResponse
        text: ""
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        y:100
        font.pointSize: 12
        font.bold: true
        color: "green"
    }
}
