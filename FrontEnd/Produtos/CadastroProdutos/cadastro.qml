import QtQuick 2.0
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "../../sharedDefinitions.js" as sharedDefinitions

Rectangle {
    color: sharedDefinitions.bgColor
    border.color: "black"
    border.width: 2
    property bool editar:false;
    property bool excluir:false;
    property string editarID:"";
    property var papeis;

    Component.onCompleted: {
        papeis = JSON.parse(window.sessionStorage.getItem('papeis'));
        if(!papeis)
            window.sessionStorage.setItem('getPapeis','false');
        else
            window.sessionStorage.removeItem('getPapeis');

        socket.active = true;
    }

    Component.onDestruction: socket.active = false;

    function adicionarOutrosNomes(nome)
    {
        var existe=false;
        for(var i = 0; i < listModelNomes.count; ++i)
            if (listModelNomes.get(i).nome===nome)
                existe=true;
        if(existe)
            erroOutrosNomes.text=nome+" já foi informado."
        else
        listModelNomes.append({
                                  "nome": nome
                              })
    }

    WebSocket {
        id: socket
        url: sharedDefinitions.wsProdutoURL
        active: true
        onTextMessageReceived: {
            if(editar)
            {
                editar = false;
                var obj = JSON.parse(message);

                inNome.text=obj.Nome;
                inRiscos.text=obj.Riscos;
                inPSocorros.text=obj.PSocorros;
                cbPF.checked=obj.PF;
                cbE.checked=obj.Exercito;
                inH1.text=obj.H1;
                inH2.text=obj.H2;
                inH3.text=obj.H3;
                inH4.text=obj.H4;

                var splitON = obj.OutrosNomes.split(",");
                for(var i = 0; i<splitON.length;i++)
                {
                    adicionarOutrosNomes(splitON[i]);
                }
            }
            else if(message==="Ok")
            {
                window.sessionStorage.setItem('responseProduto', "Reagente "+inNome.text+
                                              (excluir?" excluído ": editarID.length>0?" atualizado ": " cadastrado ") +
                                                                            "com sucesso.");
                window.location="../";
            }
            else
            {
                txtResponse.text=message;
                txtResponse.visible=true;
            }
        }
        onStatusChanged: {if(socket.status===WebSocket.Open){
                var resp = window.sessionStorage.getItem('editarProduto');
                if(resp)
                {
                    editarID=resp;
                    editar=true;

                    socket.sendTextMessage(JSON.stringify({"Acao":"Buscar",
                                                           "ID":editarID,
                                                           "token":window.sessionStorage.getItem('token')}));
                }
                window.sessionStorage.removeItem('editarProduto');
            }
        }
    }

    Text {
        text: "Cadastro de Reagentes"
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 24
        font.bold: true
    }
    ListModel {
        id: listModelNomes
    }

    Grid {
        id: grid
        x: 10
        y: 50
        width: parent.width
        columns: 2
        spacing: 5

        Text {
            text: "Nome"
        }

        TextInput {
            id: inNome
            width: 100
            focus: true
        }
        Text {
            text: "Outros nomes"
        }

        Column {
            Row {
                spacing: 5
                TextInput {
                    id: inOutrosNomes
                    width: 100
                }
                Button {
                    text: "Adicionar"
                    onClicked: adicionarOutrosNomes(inOutrosNomes.text);
                }
                Text{
                    id:erroOutrosNomes
                    visible:false
                    onTextChanged: {if(text!=="")erroOutrosNomesAnim.restart();}
                    color: "red"
                    NumberAnimation on visible {id:erroOutrosNomesAnim; from:1; to:0;duration: 1000;onRunningChanged: if(!running)erroOutrosNomes.text="";}
                }
            }
        }
        Text {
            text: " "
        }
        Column {
            Text {
                visible: listaNomes.count == 0
                text: " "
            }
            ListView {
                id: listaNomes
                model: listModelNomes
                delegate: Component {
                    id: nomeDelegate

                    Rectangle {
                        id: nomeDelegateRect
                        width: 180
                        height: 20
                        border.color: "black"
                        Column {
                            Row {
                                Text {
                                    text: nome
                                }
                                Button {
                                    text: "Remover"
                                    anchors.left: nomeDelegateRect.right
                                    onClicked: {
                                        listModelNomes.remove(index)
                                    }
                                }
                            }
                        }
                    }
                }
                highlight: Rectangle {
                    width: 180
                    height: 40
                    color: "lightsteelblue"
                    radius: 5
                }
                focus: true
            }
        }

        Text {
            text: "Controlado por"
        }

        Column{
            Row{
            CheckBox{
                id: cbPF
                width: 150
                text: "Polícia Federal"
            }
            CheckBox{
                id: cbE
                width: 150
                text: "Exército"
            }
            }
        }

        Text {
            text: "Riscos"
        }

        TextArea {
            id: inRiscos
            width: 500<parent.parent.width-300?500:parent.parent.width-300;
            height: 100
        }

        Text {
            text: "Primeiros Socorros"
        }

        TextArea {
            id: inPSocorros
            width: 500<parent.parent.width-300?500:parent.parent.width-300;
            height: 100
        }
    }
    Rectangle
    {
        id:diamanteHommel
        y:grid.height/2
        x:inPSocorros.x+inPSocorros.width+30
        Rectangle {
            id: dHV
            x: Math.sqrt(width * width + height * height) / 2
            rotation: 45
            color: "red"
            width: 50
            height: 50
            TextInput {
                id: inH1
                anchors.centerIn: parent
                rotation: -45
                width: 10
                maximumLength: 1
            }
        }
        Rectangle {
            y: dHV.y + Math.sqrt(width * width + height * height) / 2
            rotation: 45
            color: "blue"
            width: 50
            height: 50
            TextInput {
                id:inH2
                anchors.centerIn: parent
                rotation: -45
                width: 10
                maximumLength: 1
            }
        }
        Rectangle {
            x: Math.sqrt(width * width + height * height)
            y: dHV.y + Math.sqrt(width * width + height * height) / 2
            rotation: 45
            color: "yellow"
            width: 50
            height: 50
            TextInput {
                id:inH3
                anchors.centerIn: parent
                rotation: -45
                width: 10
                maximumLength: 1
            }
        }
        Rectangle {
            id: dHW
            x: Math.sqrt(width * width + height * height) / 2
            y: dHV.y + Math.sqrt(width * width + height * height)
            rotation: 45
            color: "white"
            width: 50
            height: 50
            TextInput {
                id:inH4
                anchors.centerIn: parent
                rotation: -45
                width: 10
                maximumLength: 1
            }
        }
    }

    Text {
        id:txtResponse
        text: ""
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        y:botoes.y-20
        font.pointSize: 12
        font.bold: true
        color: "red"
    }

    Row{
        id: botoes
        y: grid.y + grid.height + 20
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 5
        Button {
            text: "Salvar"
            enabled: inNome.text.length>0
            onClicked: {//socket.active = true;
                var datamodel = [];
                for (var i = 0; i < listModelNomes.count; ++i)
                  datamodel.push(listModelNomes.get(i));

                var keysList = JSON.stringify({"Acao":"Cadastrar",
                                               "IDProduto":editarID,
                                               "Nome":inNome.text,
                                               "OutrosNomes":datamodel,
                                               "PF":cbPF.checked,
                                               "Exercito":cbE.checked,
                                               "Riscos":inRiscos.text,
                                               "PSocorros":inPSocorros.text,
                                               "H1":inH1.text,
                                               "H2":inH2.text,
                                               "H3":inH3.text,
                                               "H4":inH4.text,
                                               "token":window.sessionStorage.getItem('token')});

                socket.sendTextMessage(keysList);
            }
        }
        Button {
            text: "Excluir"
            visible: editarID.length>0
            onClicked: {excluir = true;
                socket.sendTextMessage(JSON.stringify({"Acao":"Excluir",
                                                       "IDProduto":editarID,
                                                       "token":window.sessionStorage.getItem('token')}));
                    }
        }
        Button {
            text: "Cancelar"
            onClicked: window.location = "../";
        }
    }
}
