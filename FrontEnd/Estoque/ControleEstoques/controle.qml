import QtQuick 2.12
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "../../sharedDefinitions.js" as sharedDefinitions

Rectangle {
    id:telaControle
    color: sharedDefinitions.bgColor
    border.color: "black"
    border.width: 2
    property string idEstoque:"";
    property int QtdLista:0;
    property var papeis;
    property var produtoGridSize:100;

    Component.onCompleted: {
        papeis = JSON.parse(window.sessionStorage.getItem('papeis'));
        if(!papeis)
            window.sessionStorage.setItem('getPapeis','false');
        else
            window.sessionStorage.removeItem('getPapeis');
    }

    Component.onDestruction: {
        socket.active = false;
        socketProduto.active = false;
    }

    WebSocket {
        id: socket
        url: sharedDefinitions.wsEstoqueURL
        active: false
        onTextMessageReceived: {
            try {
                var obj = JSON.parse(message);

                console.log(message);
                if(obj.Response=="Buscar"){
                    txtNome.text+=obj.Nome;
                    if(obj.Local)
                        txtLocal.text+=obj.Local;
                }else if(obj.Response=="ListarProdutos"){
                    listModelProdutos.clear();
                    for (var i = 0; i < obj.Produtos.length; i++){
                        obj.Produtos[i]["Nome"]=findProdutoNome(obj.Produtos[i].IDProduto);
                        listModelProdutos.append(obj.Produtos[i]);
                        console.log(JSON.stringify(obj.Produtos[i]));
                    }
                    QtdLista = obj.Quantidade;
                }else if(obj.Response=="ListarPrateleiras"){
                    listModelPrateleiras.clear();
                    for (var i = 0; i < obj.Prateleiras.length; i++){
                        listModelPrateleiras.append(obj.Prateleiras[i]);
                        console.log(JSON.stringify(obj.Prateleiras[i]));
                    }
                }else if(obj.Response=="ListarMovimentacao"){
                    listModelMovimentacao.clear();
                    for (var i = 0; i < obj.Movimentacao.length; i++){
                        listModelMovimentacao.append(obj.Movimentacao[i]);
                    }
                }
            } catch(err) {
                if(message==="CadastrarProduto-Ok") {
                    txtResponse.text = "Produto adicionado com sucesso."
                    txtResponse.color = "green";
                } else if(message==="AlterarProduto-Ok") {
                    txtResponse.text = "Produto atualizado com sucesso."
                    txtResponse.color = "green";
                } else if(message==="RemoverSaldo-Ok") {
                    txtResponse.text = "Saldo atualizado com sucesso."
                    txtResponse.color = "green";
                } else {
                    txtResponse.text=message;
                    txtResponse.color = "red";
                }
                txtResponse.visible=true;
            }
        }
        onStatusChanged: { if(socket.status===WebSocket.Open){
                var resp = window.sessionStorage.getItem('idEstoque');
                if(resp && idEstoque=="")
                {
                    idEstoque=resp;
                    editar=true;

                    socket.sendTextMessage(JSON.stringify({"Acao":"Buscar",
                                                           "ID":idEstoque,
                                                           "token":window.sessionStorage.getItem('token')}));
                    socket.sendTextMessage(JSON.stringify({"Acao":"ListarProdutos",
                                                           "IDEstoque":idEstoque,
                                                           "token":window.sessionStorage.getItem('token')}));
                }
            }
            else if(socket.status===WebSocket.Error){
                window.sessionStorage.setItem('responseEstoque', "Serviço de estoque não disponível");
                window.location="../";
            }
        }
    }

    WebSocket {
        id: socketProduto
        url: sharedDefinitions.wsProdutoURL
        active: true
        onTextMessageReceived: {
            try {
            var obj = JSON.parse(message);
                console.log(message);
            var produtos = [];
            for (var i = 0; i < obj.lista.length; i++){
                produtos.push(obj.lista[i].Nome);
                lmProdutos.append(obj.lista[i]);
            }

            cbProduto.model=produtos;

            active=false;
            socket.active = true;
            } catch (err) {

            }

        }
        onStatusChanged: if(status===WebSocket.Open) sendTextMessage(JSON.stringify({"Acao":"Listar","Filtro":"","token":window.sessionStorage.getItem('token')}));
    }

    function findProdutoID(nome)
    {
        if(nome==="")
            return ;
        for(var i = 0; i < lmProdutos.count;i++)
        {
            if(lmProdutos.get(i).Nome===nome)
                return lmProdutos.get(i).IDProduto;
        }
        return 0;
    }
    function findProdutoNome(id)
    {
        for(var i = 0; i < lmProdutos.count;i++)
        {
            if(lmProdutos.get(i).IDProduto===id)
                return lmProdutos.get(i).Nome;
        }
        return "Reagente";
    }
    function countProdutosPrateleira(prat) {
        for(var i = 0; i < listModelPrateleiras.count;i++)
        {
            if(listModelPrateleiras.get(i).Prateleira===prat)
            {
                return Number(listModelPrateleiras.get(i).Qtd);
            }
        }
        return 0;
    }

    Text {
        text: "Controle de Estoque"
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 24
        font.bold: true
    }

    Rectangle{
        color: "black"
        anchors.fill: parent
        visible: modalProduto.visible
        z:5
        opacity: 0.7
    }

    function clearModalProduto(){
        cbProduto.currentIndex = 0;
        inPrat.text = "";
        rbIdent.checked = true;
        inQtd.text = "";
        inObs.text = "";
    }
    Rectangle{
        id:modalProduto
        anchors.centerIn: parent
        width: 330
        height: 350
        border.width: 2
        visible: false
        z:10
        property bool editar:false;
        property int IDProdutoEstoque:0;

        onVisibleChanged: {
            if(visible)
                socket.sendTextMessage(JSON.stringify({"Acao":"ListarMovimentacao",
                                                   "IDProdutoEstoque":modalProduto.IDProdutoEstoque,
                                                   "token":window.sessionStorage.getItem('token')}));
            else clearModalProduto();
        }

        Column{
            y:5
            spacing: 5
            Row{
                spacing: 5
                Text {
                    text: "<b>Reagente:</b>"
                }
                ComboBox{
                    id:cbProduto
                    visible: !modalProduto.editar
                }
                Text{
                    text:cbProduto.currentText
                    visible: modalProduto.editar
                }
            }
            Row {
                spacing: 5
                visible: modalProduto.editar
                Text{
                    text:"Obs:"
                    font.bold: true
                }
                TextArea{
                    id:inObs
                    width: 250
                    height: 50
                }
            }
            Row{
                spacing: 5
                Column{
                    spacing: 5
                    Row{
                        spacing: 5
                        Text {
                            text: "<b>Prateleira:</b>"
                        }
                        TextInput{
                            id:inPrat
                            width: 100
                            onTextChanged: {
                                if(text.toString().trim()!==""&&
                                        text.toString().toUpperCase().trim().match("\\D") !== null)
                                    text=text.toString().toUpperCase().trim().match("\\w[0-9]*")[0];
                                else if(text.toString().trim()!=="")
                                    text = ""

                                inIdent.text=rbIdent.checked&&inPrat.text.length?(countProdutosPrateleira(inPrat.text)+1)+inPrat.text:"";
                            }
                        }
                    }
                    Row{
                        spacing: 5
                        Text {
                            text: "<b>Itendificação:</b>"
                        }
                        TextInput{
                            id:inIdent
                            width: 100
                            readOnly: rbIdent.checked
                            text:rbIdent.checked&&inPrat.text.length?(countProdutosPrateleira(inPrat.text)+1)+inPrat.text:"";
                        }
                        CheckBox {
                            id: rbIdent
                            text: "Auto"
                            checked: true
                        }
                    }
                }
                Button {
                    anchors.verticalCenter: parent.verticalCenter
                    visible: modalProduto.editar
                    text: "Alterar"
                    onClicked: {
                        if(confirm("Deseja atualizar os dados de Identificação e Prateleira do reagente "+cbProduto.currentText+"?"))
                        {
                            if(inObs.text===""&&confirm("Não foi informado um motivo ou observação."))
                            {
                                socket.sendTextMessage(JSON.stringify({"Acao":"AlterarProduto",
                                                                          "ID":modalProduto.IDProdutoEstoque,
                                                                          "Identificacao":inIdent.text.toString().toLocaleUpperCase(),
                                                                          "Prateleira":inPrat.text.toString().toLocaleUpperCase(),
                                                                          "Observacao":inObs.text,
                                                                          "token":window.sessionStorage.getItem('token')}));

                                buscar();
                                modalProduto.visible = false;
                            }
                        }
                    }
                }
            }
            Row{
                spacing: 5
                Text {
                    text: "<b>Qtd:</b>"
                }
                TextInput{
                    id:inQtd
                    width: 100
                    onTextChanged: {
                        if(text.toString().trim()!==""&&text.toString().trim().match("[0-9]+(,|\\.)?[0-9]*")&&text!==text.toString().trim().match("[0-9]+(,|\\.)?[0-9]{0,4}")[0])
                            text=Number(text.toString().trim().match("[0-9]+(,|\\.)?[0-9]{0,4}")[0]);
                        else if(text.toString().trim()!==""&&!text.toString().toUpperCase().trim().match("[0-9]+(,|\\.)?[0-9]*"))
                            text = ""
                    }
                }
                ComboBox{
                    id:cbUnidade
                    model:["g","kg","ml","l"]
                    visible: !modalProduto.editar
                }
                Text{
                    text:cbUnidade.currentText
                    visible: modalProduto.editar
                }
                Button {
                    text:"Remover"
                    visible: modalProduto.editar
                    onClicked: {
                        if(confirm("Deseja remover "+inQtd.text+cbUnidade.currentText+" do reagente "+cbProduto.currentText+"?"))
                        {
                            if(inObs.text===""&&confirm("Não foi informado um motivo ou observação."))
                            {
                                socket.sendTextMessage(JSON.stringify({"Acao":"RemoverSaldo",
                                                                       "Quantidade":inQtd.text.toString().replace(',','.'),
                                                                       "Observacao":"",
                                                                       "IDProdutoEstoque":modalProduto.IDProdutoEstoque,
                                                                       "token":window.sessionStorage.getItem('token')}));
                                buscar();
                                modalProduto.visible=false;
                            }
                        }
                    }
                }
            }
            Column{
                anchors.horizontalCenter: parent.horizontalCenter
                visible: modalProduto.editar
                Text {
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.bold: true
                    text: "Movimentação";
                }

                ScrollView{
                    anchors.horizontalCenter: modalProduto.horizontalCenter
                    width: modalProduto.width-10
                    height: 130
                    verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn
                    contentItem:Column{
                        ListView{
                            model: listModelMovimentacao
                            delegate: Rectangle{
                                height: 20
                                width: 300
                                color: "transparent"
                                Text {
                                      property date dt: new Date(Data*1000)
                                      text: (Movimentacao>=0?'<font color=\"blue\">+':'<font color=\"red\">')+Movimentacao + "</font> | " +
                                            (SaldoFinal>=0?'<font color=\"blue\">':'<font color=\"red\">')+"Saldo: " + Number(SaldoFinal.toString().trim().match("[0-9]+(,|\\.)?[0-9]{0,4}")[0])+"</font> | "+
                                            dt.toLocaleDateString() + " " + dt.toLocaleTimeString();
                                  }
                            }
                        }
                    }
                }
            }
        }

        Column{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            spacing: 5

            Button {
                anchors.horizontalCenter: parent.horizontalCenter
                visible: !modalProduto.editar
                text: "Cadastrar"
                onClicked: {
                    socket.sendTextMessage(JSON.stringify({"Acao":"CadastrarProduto",
                                                              "ID":modalProduto.IDProdutoEstoque,
                                                              "Identificacao":inIdent.text.toString().toLocaleUpperCase(),
                                                              "IDProduto":findProdutoID(cbProduto.currentText),
                                                              "IDEstoque":idEstoque,
                                                              "Prateleira":inPrat.text.toString().toLocaleUpperCase(),
                                                              "Quantidade":inQtd.text.toString().replace(',','.'),
                                                              "Unidade":cbUnidade.currentText,
                                                              "token":window.sessionStorage.getItem('token')}));
                    socket.sendTextMessage(JSON.stringify({"Acao":"ListarProdutos",
                                                           "IDEstoque":idEstoque,
                                                           "token":window.sessionStorage.getItem('token')}));
                    clearModalProduto();
                    modalProduto.visible = false;
                }
                enabled: typeof cbProduto.currentText!="undefined"&&cbProduto.currentText.toString().trim()!=""&&
                         cbUnidade.currentText!=""&&
                         inPrat.text!=""&&
                         inIdent.text!=""&&
                         inQtd.text.toString().trim().match("[0-9]+(,|\\.)?[0-9]*");
            }
            Button {
                text: "Cancelar"
                onClicked: {
                    clearModalProduto();
                    modalProduto.visible = false;
                }
            }
        }
    }

    Rectangle {
        x: 10
        y: 50
        width: txtNome.width+txtLocal.width+btnEditar.width+10
        height: 20
        anchors.horizontalCenter: parent.horizontalCenter
        color: sharedDefinitions.bgColor
        border.color: "black"

        Text {
            id: txtNome
            text: "<b>Nome: </b>"
        }

        Text {
            id: txtLocal
            anchors.left: txtNome.right
            anchors.leftMargin: 5
            text: "<b>Local: </b>"
        }

        Rectangle {
            id:btnEditar
            anchors.left: txtLocal.right
            anchors.leftMargin: 5
            width: 20
            height: 20
            border.color: "black"
            z:1
            Image {
                anchors.centerIn: parent
                width: parent.width*0.6
                height: parent.height*0.6
                source: "../../img/edit.svg"
            }
            MouseArea{
                anchors.fill: parent
                onEntered: parent.impl.style.background = 'linear-gradient(LightBlue, GhostWhite)';
                onExited: parent.impl.style.background = '';
                onClicked: {
                    window.location = "../CadastroEstoques";}
            }
        }
    }
    ListModel {
        id: lmProdutos
        onCountChanged: applyFilter();
    }
    ListModel {
        id: listModelProdutos
        onCountChanged: applyFilter();
    }
    ListModel {
        id: listModelMovimentacao
    }
    ListModel {
        id: listModelPrateleiras
    }
    ListModel {
        id: displayModelProdutos
    }

    function applyFilter() {
        displayModelProdutos.clear();
        for(var i = 0; i < listModelProdutos.count;i++)
        {
            var nome = (typeof listModelProdutos.get(i).Nome === "undefined")?findProdutoNome(listModelProdutos.get(i).ID):listModelProdutos.get(i).Nome;

            if(inFiltro.text.trim()===""
                    ||(cbFiltro.currentText==="Reagente"&&(nome.toLowerCase().indexOf(inFiltro.text.toLowerCase().trim())>=0))
                    ||(cbFiltro.currentText==="Prateleira"&&(listModelProdutos.get(i).Prateleira.toLowerCase().indexOf(inFiltro.text.toLowerCase().trim())>=0))
                    ||(cbFiltro.currentText==="Identificação"&&(listModelProdutos.get(i).Identificacao.toLowerCase().indexOf(inFiltro.text.toLowerCase().trim())>=0)))
                displayModelProdutos.append(listModelProdutos.get(i));
        }

        var max=0;
        for(var i = 0; i < displayModelProdutos.count;i++)
        {
            if(displayModelProdutos.get(i).Nome.trim().length>max)
                max = displayModelProdutos.get(i).Nome.trim().length;
        }
        produtoGridSize=max;
    }
    function buscar(){
        socket.sendTextMessage(JSON.stringify({"Acao":"ListarProdutos",
                                               "IDEstoque":idEstoque,
                                               "TipoFiltro":cbFiltro.currentText,
                                               "Filtro":cbFiltro.currentText==="Reagente"?findProdutoID(inFiltro.text):inFiltro.text,
                                               "token":window.sessionStorage.getItem('token')}));
    }

    Rectangle {
        x: 10
        y: 80
        width: parent.width-10
        height: 200
        anchors.horizontalCenter: parent.horizontalCenter
        border.color: "black"

        Column{
            spacing: 10
            anchors.horizontalCenter: parent.horizontalCenter
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 30
                Text {
                    text: "Reagentes"
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pointSize: 14
                    font.bold: true
                }

                Rectangle{
                    color: "white"
                    y:2
                    width: 20
                    height: 20
                    border.color: "black"
                    border.width: 1
                    radius: 3

                    Image {
                        anchors.centerIn: parent
                        width: parent.width*0.5
                        height: parent.height*0.5
                        source: "../../img/plus.svg";
                    }
                    MouseArea{
                        anchors.fill: parent
                        onEntered: parent.impl.style.background = 'linear-gradient(LightBlue, GhostWhite)';
                        onExited: parent.impl.style.background = '';
                        onClicked: {
                            modalProduto.IDProdutoEstoque=0;
                            modalProduto.visible=true;
                            modalProduto.editar=false;
                        }
                    }
                }
            }

            Grid {
                id:filtros
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: 5
                columns: 1+Math.min(1,telaControle.width/400);

                Rectangle{
                    width: 260
                    height: 20
                    color: "transparent"
                    Row{
                        spacing: 50
                        anchors.horizontalCenter: parent.horizontalCenter
                        ComboBox{
                            id: cbFiltro
                            width: 100
                            model:["Reagente", "Prateleira", "Identificação"]
                        }

                        TextInput{
                            id:inFiltro
                            width: 200
                            anchors.right: parent.parent.right
                            onTextChanged: applyFilter();
                        }
                    }
                }
                Rectangle{
                    anchors.verticalCenter: parent.verticalCenter + (filtros.columns==2?0:15);
                    anchors.horizontalCenter: parent.horizontalCenter + (filtros.columns==1?0:115);
                    width: 100
                    height: 20
                    color: "transparent"
                    Button {
                        anchors.centerIn: parent
                        text: "Buscar"
                        onClicked: {
                            buscar();
                        }
                    }
                }
            }

            Grid{
                id:gridProdutos
                anchors.horizontalCenter: parent.horizontalCenter
                columns: 1+Math.min(Math.sqrt(displayModelProdutos.count-1)*1.5,telaControle.width/(produtoGridSize*6+200));
                onHeightChanged: {
                    document.body.style.height=300+height+"px";
                    telaControle.height=300+height;
                    parent.height = 150+height;
                }
                ListView {
                    id: listaProdutos
                    model: displayModelProdutos
                    orientation: Qt.Horizontal
                    Component.onCompleted: orientation=Qt.Horizontal;
                    delegate: Component {
                        Rectangle{
                            width: produtoGridSize*12/2+20;
                            height: 60
                            Rectangle{
                                id:lwRect
                                anchors.horizontalCenter: parent.horizontalCenter
                                color: "white"
                                width: 30
                                height: 30
                                border.color: "black"
                                border.width: 1
                                radius: 3

                                Image {
                                    anchors.centerIn: parent
                                    width: parent.width*0.5
                                    height: parent.height*0.7
                                    source: "../../img/erlenmeyer.png";
                                }
                                MouseArea{
                                    anchors.fill: parent
                                    onEntered: parent.impl.style.background = 'linear-gradient(LightBlue, GhostWhite)';
                                    onExited: parent.impl.style.background = '';
                                    enabled: !modalProduto.visible
                                    onClicked: {
                                        cbProduto.currentText=Nome;
                                        inPrat.text=Prateleira;
                                        rbIdent.checked=true;
                                        inIdent.text=Identificacao;
                                        inQtd.text=Quantidade;
                                        cbUnidade.currentText=Unidade;

                                        modalProduto.IDProdutoEstoque=ID;
                                        modalProduto.editar=true;
                                        modalProduto.visible=true;
                                    }
                                }
                            }
                            Text {
                                id: lwNome
                                text: Nome+"\n"+Identificacao
                                font.bold: true
                                font.pointSize: 9
                                anchors.top: lwRect.bottom
                                anchors.horizontalCenter: lwRect.horizontalCenter
                            }
                        }
                    }
                    highlight: Rectangle {
                        width: 180
                        height: 40
                        color: "lightsteelblue"
                        radius: 5
                    }
                    focus: true
                }
            }
            Text{
                anchors.horizontalCenter: parent.horizontalCenter
                text:"listados "+displayModelProdutos.count+" de "+QtdLista;
            }
        }
    }


    Text {
        id:txtResponse
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 40
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 12
        font.bold: true
        color: "red"
        onVisibleChanged: timerResponse.start();
        Timer{
            id: timerResponse
            interval: 30*txtResponse.text.length
            onRunningChanged: {
                if(!running)
                    txtResponse.visible=false;
            }
        }
    }

    Button {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        text: "Cancelar"
        onClicked: window.location = "../";
        enabled: !modalProduto.visible
    }
}
