import QtQuick 2.0
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "../../sharedDefinitions.js" as sharedDefinitions

Rectangle {
    color: sharedDefinitions.bgColor
    border.color: "black"
    border.width: 2
    property bool editar:false;
    property bool excluir:false;
    property string editarID:"";
    property var papeis;

    Component.onCompleted: {
        papeis = JSON.parse(window.sessionStorage.getItem('papeis'));
        if(!papeis)
            window.sessionStorage.setItem('getPapeis','false');
        else
            window.sessionStorage.removeItem('getPapeis');

        socket.active = true;
    }

    Component.onDestruction: socket.active = false;

    WebSocket {
        id: socket
        url: sharedDefinitions.wsEstoqueURL
        active: true
        onTextMessageReceived: {
            if(editar)
            {
                editar = false;
                var obj = JSON.parse(message);

                inNome.text=obj.Nome;
                inLocal.text=obj.Local;
                cbInstitucional.checked=obj.Tipo==="Institucional";
                cbPessoal.checked=obj.Tipo==="Pessoal";
                inObs.text=obj.Obs;
            }
            else if(message==="Ok")
            {
                window.sessionStorage.setItem('responseEstoque', "Estoque "+inNome.text+
                                              (excluir?" excluído ": editarID.length>0?" atualizado ": " cadastrado ") +
                                                                            "com sucesso.");
                window.location="../";
            }
            else
            {
                txtResponse.text=message;
                txtResponse.visible=true;
            }
        }
        onStatusChanged: {if(socket.status===WebSocket.Open){
                var resp = window.sessionStorage.getItem('idEstoque');
                if(resp)
                {
                    editarID=resp;
                    editar=true;

                    socket.sendTextMessage(JSON.stringify({"Acao":"Buscar",
                                                           "ID":editarID,
                                                           "token":window.sessionStorage.getItem('token')}));
                }
                window.sessionStorage.removeItem('idEstoque');
            }
            else if(socket.status===WebSocket.Error){
                window.sessionStorage.setItem('responseEstoque', "Serviço de estoque não disponível");
                window.location="../";
            }
        }
    }

    Text {
        text: "Cadastro de Estoque"
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 24
        font.bold: true
    }

    Grid {
        id: grid
        x: 10
        y: 50
        width: parent.width
        columns: 2
        spacing: 5

        Text {
            text: "Nome"
        }

        TextInput {
            id: inNome
            width: 100
            focus: true
        }

        Text {
            text: "Local"
        }

        TextInput {
            id: inLocal
            width: 500
            focus: true
        }

        Text {
            text: "Tipo"
        }

        Row{
            spacing: 5
            CheckBox {
                id:cbInstitucional
                width: 100
                text: "Institucional"
                Component.onCompleted: {impl.children[0].disabled=papeis.almoxarifado.roles.indexOf("CRUD_EstoqueInstitucional")<0;}
                onCheckedChanged: cbPessoal.checked = !checked;
            }
            CheckBox {
                id:cbPessoal
                text: "Pessoal"
                checked: true
                Component.onCompleted: {impl.children[0].disabled=papeis.almoxarifado.roles.indexOf("CRUD_EstoqueInstitucional")<0;}
                onCheckedChanged: cbInstitucional.checked = !checked;
            }
        }

        Text {
            text: "Obs"
        }

        TextArea {
            id: inObs
            width: 500
            height: 100
        }
    }

    Text {
        id:txtResponse
        text: ""
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        y:botoes.y-20
        font.pointSize: 12
        font.bold: true
        color: "red"
    }

    Row{
        id: botoes
        y: grid.y + grid.height + 20
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 5
        Button {
            text: "Salvar"
            enabled: inNome.text.length>0
            onClicked: {
                var keysList = JSON.stringify({"Acao":"Cadastrar",
                                               "IDEstoque":editarID,
                                               "Nome":inNome.text,
                                               "Local":inLocal.text,
                                               "Tipo":cbInstitucional.checked?"Institucional":"Pessoal",
                                               "Obs":inObs.text,
                                               "token":window.sessionStorage.getItem('token')});

                socket.sendTextMessage(keysList);
            }
        }
        Button {
            text: "Excluir"
            visible: editarID.length>0
            onClicked: {excluir = true;
                socket.sendTextMessage(JSON.stringify({"Acao":"Excluir",
                                                       "IDEstoque":editarID,
                                                       "token":window.sessionStorage.getItem('token')}));
                    }
        }
        Button {
            text: "Cancelar"
            onClicked: window.location = "../";
        }
    }
}
