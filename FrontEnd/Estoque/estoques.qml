import QtQuick 2.0
import QtQuick.Controls 1.4
import QtWebSockets 1.1
import "../sharedDefinitions.js" as sharedDefinitions

Rectangle {
    id:telaEstoque
    color: sharedDefinitions.bgColor
    border.color: "black"
    border.width: 2
    property var papeis;
    property bool servicoOnline: socket.status === WebSocket.Open|| socket.status === WebSocket.Closed;

    Component.onCompleted: {
        var response = window.sessionStorage.getItem('responseEstoque');

        if(response)
        {
            txtResponse.text=response;
            txtResponse.visible=true;
        }
        window.sessionStorage.removeItem('responseEstoque');

        papeis = JSON.parse(window.sessionStorage.getItem('papeis'));
        if(!papeis)
            window.sessionStorage.setItem('getPapeis','false');
        else
            window.sessionStorage.removeItem('getPapeis');

        listModelNomes.append({"Nome":"Novo","Tipo":"Adicionar","IDEstoque":""});
    }

    WebSocket {
        id: socket
        url: sharedDefinitions.wsEstoqueURL
        active: false
        onTextMessageReceived: {
            if(message.indexOf("lista")>0)
            {
                var obj = JSON.parse(message);
                listModelNomes.clear();
                for (var i = 0; i < obj.lista.length; i++) {
                    listModelNomes.append({"Nome":obj.lista[i].Nome,"Tipo":"",
                    "IDEstoque":obj.lista[i].IDEstoque});
                }
                listModelNomes.append({"Nome":"Novo","Tipo":"Adicionar","IDEstoque":""});
            }

            socket.active=false;
        }
        onStatusChanged: {
            servicoOnline= socket.status === WebSocket.Open|| socket.status === WebSocket.Closed;
            if(socket.status===WebSocket.Open){
                socket.sendTextMessage(JSON.stringify({"Acao":"Listar","token":window.sessionStorage.getItem('token')}));
            }
            else if(socket.status===WebSocket.Error){
                txtResponse.text = "Serviço de estoques não disponível";
                txtResponse.color = "red";
                txtResponse.visible = true;
            }
        }
        Component.onCompleted: active = true;
    }

    Text {
        text: "Estoques"
        anchors.horizontalCenter: parent.horizontalCenter
        font.pointSize: 24
        font.bold: true
    }

    ListModel {
        id: listModelNomes
    }
    Grid{
        id:gridEstoques
        anchors.horizontalCenter: parent.horizontalCenter
        y:100
        spacing: 20
        columns: 1+Math.min(Math.sqrt(listModelNomes.count-1)*1.5,telaEstoque.width/220);
        onHeightChanged: {document.body.style.height=120+height+"px";telaEstoque.height=120+height;}
        ListView {
            id: listaNomes
            model: listModelNomes
            orientation: Qt.Horizontal
            spacing: 20
            Component.onCompleted: orientation=Qt.Horizontal;
            delegate: Component {
                    Rectangle{
                        color: "white"
                        width: 100
                        height: 100
                        border.color: "black"
                        border.width: 3
                        radius: 10

                        Image {
                            anchors.centerIn: parent
                            width: parent.width*0.7
                            height: parent.height*0.7
                            source: Tipo==="Adicionar"?"../img/plus.svg":"../img/estante.svg";
                        }
                        MouseArea{
                            anchors.fill: parent
                            onEntered: parent.impl.style.background = servicoOnline?'linear-gradient(LightBlue, GhostWhite)':'linear-gradient(LightPink, GhostWhite)';
                            onExited: parent.impl.style.background = '';
                            onClicked: {
                                if(!servicoOnline)
                                {
                                    parent.impl.style.background = 'linear-gradient(LightPink, GhostWhite)';
                                    return;
                                }

                                if(IDEstoque!=="")
                                    window.sessionStorage.setItem('idEstoque',IDEstoque);
                                window.location = Tipo==="Adicionar"?"/Estoque/CadastroEstoques":"/Estoque/ControleEstoques";
                            }
                        }
                        Text {
                            text: Nome
                            font.bold: true
                            anchors.top: parent.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }
            }
            highlight: Rectangle {
                width: 180
                height: 40
                color: "lightsteelblue"
                radius: 5
            }
            focus: true
        }
    }

    Text {
        id:txtResponse
        text: ""
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
        y:50
        font.pointSize: 12
        font.bold: true
        color: "green"
    }
}
