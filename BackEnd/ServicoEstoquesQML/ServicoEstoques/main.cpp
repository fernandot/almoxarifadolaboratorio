#include <QGuiApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.setOfflineStoragePath("/Database_Almoxarifado");
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    QStringList args = QCoreApplication::arguments();
    int port = 0;
    for(int c=args.size()-1;c>=0;--c)
    {
        QString arg = args.at(c);
        port = arg.toInt();
    }

    QMetaObject::invokeMethod(engine.rootObjects()[0], "setPort",  Q_ARG( QVariant, port ));

    return app.exec();
}
