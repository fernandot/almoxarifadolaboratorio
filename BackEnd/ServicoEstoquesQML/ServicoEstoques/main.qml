import QtQuick 2.0
import QtQuick.Window 2.11
import QtWebSockets 1.1
import QtQuick.LocalStorage 2.0
import QtQml 2.11
import "../../Utils/decodeToken.js" as DT;
import "../../Utils/servicosConfig.js" as SC;

Window {
    visible: SC.visibleEstoques
    width: 640
    height: 480
    property int serverPort: SC.pEstoques

    function setPort(port){
        if(port)
            serverPort=port;
        wsLoader.active=true;
    }

    Rectangle {
        width: 360
        height: 360
        Loader{
            id:wsLoader
            sourceComponent: wsComponent
            active: false
        }

        Component{
            id:wsComponent
            WebSocketServer {
            id: server
            listen: true
            host: "0.0.0.0"
            port: serverPort
            accept: true
            property var token;
            onPortChanged: {
                server.listen=true;
            }
            onListenChanged: {
                if(listen)
                    accept=true;
            }

            onClientConnected: {
                webSocket.onTextMessageReceived.connect(function (message) {
                    try {
                        messageBox.text = qsTr("Server received message: %1 \n").arg(message) + messageBox.text;;

                        var obj = JSON.parse(message);

                        DT.validateAccess(obj.token,"");

                        token = DT.decodeToken(obj.token);
                        messageBox.text = qsTr("token: %1 \n").arg(JSON.stringify(token)) + messageBox.text;

                        var db = LocalStorage.openDatabaseSync("Estoque", "1.0", "Banco de dados dos produtos químicos", 10000000);

                        db.transaction(
                            function(tx) {
                                tx.executeSql('CREATE TABLE IF NOT EXISTS Estoque(ID INTEGER PRIMARY KEY AUTOINCREMENT, Nome TEXT, Local TEXT, Tipo TEXT NOT NULL DEFAULT "Pessoal", Obs TEXT, Owner TEXT)');
                                tx.executeSql('CREATE TABLE IF NOT EXISTS ProdutosEstoque(ID INTEGER PRIMARY KEY AUTOINCREMENT, Identificacao TEXT NOT NULL UNIQUE, IDProduto INTEGER NOT NULL, IDEstoque INTEGER NOT NULL, Prateleira TEXT NOT NULL, Quantidade NUMBER NOT NULL, Unidade TEXT NOT NULL)');
                                tx.executeSql('CREATE TABLE IF NOT EXISTS ProdutosMovimentacao(IDProdutoEstoque INTEGER NOT NULL, Movimentacao NUMBER NOT NULL, SaldoFinal NUMBER NOT NULL, Data INTEGER NOT NULL, Observacao TEXT, Token TEXT NOT NULL)');
                                tx.executeSql('CREATE TABLE IF NOT EXISTS Log(ID INTEGER PRIMARY KEY AUTOINCREMENT, Data INTEGER, IP TEXT, Acao TEXT, Detalhes TEXT)');

                                function log(detalhe, acao){
                                    tx.executeSql('INSERT INTO Log (Data, IP, Acao, Detalhes) VALUES(?, ?, ?, ?)', [ Date.now()/1000, "", acao, detalhe ]);
                                }

                                if(obj.Acao==="Cadastrar") {
                                    if(obj.IDEstoque)
                                    {
                                        var qProd = tx.executeSql('SELECT * FROM Estoque e WHERE e.ID = ? and e.Tipo = "Institucional"', [obj.IDEstoque]);
                                        if(obj.Tipo==="Institucional"||qProd.rows.length>0)
                                            DT.validateAccess(obj.token,"CRUD_EstoqueInstitucional");

                                        tx.executeSql('DELETE FROM Estoque WHERE ID = ?',[ obj.IDEstoque ]);
                                        tx.executeSql('INSERT INTO Estoque VALUES(?, ?, ?, ?, ?, ?)', [ obj.IDEstoque, obj.Nome, obj.Local, obj.Tipo, obj.Obs, obj.Tipo==="Pessoal"?token.sub:"" ]);

                                        log(message,"Editar");
                                    }
                                    else
                                    {
                                        var qProd = tx.executeSql('SELECT e.ID FROM Estoque e WHERE e.Nome = ?', [obj.Filtro]);

                                        if(qProd.rows.length>0)
                                            throw "Estoque "+obj.Nome+" já existe.";

                                        qProd = tx.executeSql('SELECT e.ID FROM Estoque e WHERE e.Nome = ? and e.Tipo = "Institucional"', [obj.Filtro]);
                                        if(obj.Tipo==="Institucional"||qProd.rows.length>0)
                                            DT.validateAccess(obj.token,"CRUD_EstoqueInstitucional");

                                        tx.executeSql('INSERT INTO Estoque (Nome, Local, Tipo, Obs, Owner) VALUES(?, ?, ?, ?, ?)', [ obj.Nome, obj.Local, obj.Tipo, obj.Obs, obj.Tipo==="Pessoal"?token.sub:"" ]);
                                        log(message,"Cadastrar");
                                    }
                                    webSocket.sendTextMessage("Ok");
                                }
                                else if(obj.Acao==="Listar") {
                                    var qEst = tx.executeSql('SELECT e.ID as IDEstoque, e.Nome FROM Estoque e WHERE (e.Tipo = "Pessoal" and lower(e.Owner) = lower(?)) or (e.Tipo = "Institucional" and 1=?)', [token.sub, (JSON.stringify(token.resource_access.almoxarifado).indexOf("EstoqueInstitucional")>0)?1:0]);

                                    var r = [];
                                    for (var i=0; i<qEst.rows.length; i++) {
                                            r.push(qEst.rows.item(i));
                                        }
                                    webSocket.sendTextMessage(JSON.stringify({"lista":r}));
                                }
                                else if(obj.Acao==="Buscar") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e WHERE e.ID = ? and e.Tipo = "Institucional"', [obj.ID]);
                                    if(qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"EstoqueInstitucional");

                                    var qEst = tx.executeSql('SELECT "Buscar" Response, e.ID as IDEstoque, e.Nome, e.Local, e.Tipo, e.Obs FROM Estoque e WHERE e.ID = ?', [obj.ID]);

                                    webSocket.sendTextMessage(JSON.stringify(qEst.rows.item(0)));
                                }
                                else if(obj.Acao==="Excluir") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e WHERE e.ID = ? and e.Tipo = "Institucional"', [obj.IDEstoque]);
                                    if(obj.Tipo==="Institucional"||qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"CRUD_EstoqueInstitucional");

                                    if(!obj.IDEstoque)
                                        throw "Estoque não encontrado.";

                                    tx.executeSql('DELETE FROM Estoque WHERE ID = ?',[obj.IDEstoque]);
                                    log(message,"Excluir");
                                    webSocket.sendTextMessage("Ok");
                                }
                                else if(obj.Acao==="CadastrarProduto") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e WHERE e.ID = ? and e.Tipo = "Institucional"', [obj.IDEstoque]);
                                    if(qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"ControleSaldoEstoqueInstitucional");

                                    tx.executeSql('INSERT INTO ProdutosEstoque (Identificacao, IDProduto, IDEstoque, Prateleira, Quantidade, Unidade) VALUES(?, ?, ?, ?, ?, ?)', [ obj.Identificacao, obj.IDProduto, obj.IDEstoque, obj.Prateleira, obj.Quantidade, obj.Unidade ]);
                                    tx.executeSql('INSERT INTO ProdutosMovimentacao (IDProdutoEstoque, Movimentacao, SaldoFinal, Data, Token, Observacao) VALUES((SELECT MAX(ID) From ProdutosEstoque), ?, ?, ?, ?, "Cadastro")', [ obj.Quantidade, obj.Quantidade, Date.now()/1000, obj.token ]);
                                    log(message,"CadastrarProduto");
                                    webSocket.sendTextMessage("CadastrarProduto-Ok");
                                }
                                else if(obj.Acao==="AlterarProduto") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e, ProdutosEstoque pe WHERE pe.ID = ? and pe.IDEstoque = e.ID and e.Tipo = "Institucional"', [obj.ID]);
                                    if(qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"ControleSaldoEstoqueInstitucional");

                                    tx.executeSql('UPDATE ProdutosEstoque SET Identificacao = ?, Prateleira = ? WHERE ID = ?', [ obj.Identificacao, obj.Prateleira, obj.ID ]);
                                    log(message,"AlterarProduto");
                                    webSocket.sendTextMessage("AlterarProduto-Ok");
                                }
                                else if(obj.Acao==="RemoverSaldo") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e, ProdutosEstoque pe WHERE pe.ID = ? and pe.IDEstoque = e.ID and e.Tipo = "Institucional"', [obj.ID]);
                                    if(qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"ControleSaldoEstoqueInstitucional");

                                    tx.executeSql('UPDATE ProdutosEstoque SET Quantidade = (SELECT Quantidade FROM ProdutosEstoque WHERE ID = ?) - ? WHERE ID = ?', [ obj.IDProdutoEstoque, obj.Quantidade, obj.IDProdutoEstoque ]);
                                    tx.executeSql('INSERT INTO ProdutosMovimentacao (IDProdutoEstoque, Movimentacao, SaldoFinal, Data, Observacao, Token) VALUES(?, ?, (SELECT Quantidade FROM ProdutosEstoque WHERE ID = ?), ?, ?, ?)', [ obj.IDProdutoEstoque, -obj.Quantidade, obj.IDProdutoEstoque, Date.now()/1000, obj.Observacao, obj.token ]);
                                    log(message,"RemoverSaldo");
                                    webSocket.sendTextMessage("RemoverSaldo-Ok");
                                }
                                else if(obj.Acao==="ListarProdutos") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e WHERE e.ID = ? and e.Tipo = "Institucional"', [obj.IDEstoque]);
                                    if(qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"SaldoEstoqueInstitucional");

                                    var query = "SELECT * FROM ProdutosEstoque WHERE IDEstoque = "+obj.IDEstoque;
                                    if(obj.hasOwnProperty("Filtro")&&obj.Filtro!=="")
                                    {
                                        query += " and lower("
                                        switch(obj.TipoFiltro)
                                        {
                                        case "Identificação":
                                            query += "Identificacao";
                                            break;
                                        case "Reagente":
                                            query += "IDProduto";
                                            break;
                                        default:
                                            query += obj.TipoFiltro;
                                            break;
                                        }
                                        query += ") = lower(?)";
                                    }
                                    query +=" and Quantidade > 0.0001";

                                    if(obj.hasOwnProperty("Filtro")&&obj.Filtro!=="")
                                        var qEst = tx.executeSql(query,[obj.Filtro]);
                                    else
                                        var qEst = tx.executeSql(query);
                                    var r = [];
                                    for (var i=0; i<qEst.rows.length && i<50; i++) {
                                            r.push(qEst.rows.item(i));
                                        }
                                    webSocket.sendTextMessage(JSON.stringify({"Response":"ListarProdutos", "Quantidade":qEst.rows.length, "Produtos":r}));

                                    qEst = tx.executeSql('SELECT MAX(SUBSTR(Identificacao,0,INSTR(Identificacao,Prateleira))) Qtd, Prateleira FROM ProdutosEstoque pe WHERE IDEstoque = '+obj.IDEstoque+' GROUP BY pe.Prateleira');
                                    var s = [];
                                    for (var i=0; i<qEst.rows.length; i++) {
                                            s.push(qEst.rows.item(i));
                                        }
                                    webSocket.sendTextMessage(JSON.stringify({"Response":"ListarPrateleiras", "Prateleiras":s}));
                                }
                                else if(obj.Acao==="ListarMovimentacao") {
                                    var qProd = tx.executeSql('SELECT * FROM Estoque e, ProdutosEstoque pe WHERE pe.ID = ? and pe.IDEstoque = e.ID and e.Tipo = "Institucional"', [obj.IDProdutoEstoque]);
                                    if(qProd.rows.length>0)
                                        DT.validateAccess(obj.token,"SaldoEstoqueInstitucional");

                                    var qEst = tx.executeSql('SELECT * FROM ProdutosMovimentacao WHERE IDProdutoEstoque = ? ORDER BY Data DESC',[obj.IDProdutoEstoque]);
                                    var r = [];
                                    for (var i=0; i<qEst.rows.length; i++) {
                                            r.push(qEst.rows.item(i));
                                        }
                                    webSocket.sendTextMessage(JSON.stringify({"Response":"ListarMovimentacao", "Movimentacao":r}));
                                }
                            }
                        );
                    }
                    catch(err) {
                        webSocket.sendTextMessage(err);
                    }
                })
                messageBox.text = "conectou\n" + messageBox.text;
            }
            onErrorStringChanged: {
                appendMessage(qsTr("Server error: %1").arg(errorString))
                console.log(errorString);
            }
            Component.onCompleted: {
                console.log(url)
            }
        }
        }

        Text {
            id: messageBox
            anchors.fill: parent
        }
    }
}
