function decodeToken(str) {
    str = str.split('.')[1];

    str = str.replace('/-/g', '+');
    str = str.replace('/_/g', '/');
    switch (str.length % 4)
    {
        case 0:
            break;
        case 2:
            str += '==';
            break;
        case 3:
            str += '=';
            break;
        default:
            throw 'Invalid token';
    }

    str = (str + '===').slice(0, str.length + (str.length % 4));
    str = str.replace(/-/g, '+').replace(/_/g, '/');

    str = decodeURIComponent(escape(Qt.atob(str)));

    str = JSON.parse(str);
    return str;
}

function validateAccess(token, resource) {
    if(typeof token === "undefined")
        throw "Não autorizado";

    var tk = decodeToken(token);

    if(resource!=="" && (typeof tk.resource_access.almoxarifado === "undefined" || JSON.stringify(tk.resource_access.almoxarifado).indexOf(resource)<=0))
        throw "Não autorizado";

    if(tk.exp < Date.now()/1000)
        throw "Sessão expirada";

    if(typeof tk.sub === "undefined")
        throw "ID de usuário inválido";
}
