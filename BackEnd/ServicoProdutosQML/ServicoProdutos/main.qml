import QtQuick 2.0
import QtQuick.Window 2.11
import QtWebSockets 1.1
import QtQuick.LocalStorage 2.0
import QtQml 2.11
import "../../Utils/decodeToken.js" as DT;
import "../../Utils/servicosConfig.js" as SC;

Window {
    visible: SC.visibleProdutos
    width: 640
    height: 480
    property int serverPort: SC.pEstoques

    function setPort(port){
        if(port)
            serverPort=port;
        wsLoader.active=true;
    }

    Rectangle {
        width: 360
        height: 360
        Loader{
            id:wsLoader
            sourceComponent: wsComponent
            active: false
        }

        Component{
            id:wsComponent
            WebSocketServer {
            id: server
            listen: true
            host: "0.0.0.0"
            port: serverPort
            accept: true
            property var token;
            onClientConnected: {
                webSocket.onTextMessageReceived.connect(function (message) {
                    try {
                        messageBox.text = qsTr("Server received message: %1 \n").arg(message) + messageBox.text;;

                        var obj = JSON.parse(message);

                        DT.validateAccess(obj.token,"CRUD_Reagentes");

                        token = DT.decodeToken(obj.token);
                        messageBox.text = qsTr("token: %1 \n").arg(JSON.stringify(token)) + messageBox.text;

                        var db = LocalStorage.openDatabaseSync("Produtos", "1.0", "Banco de dados dos produtos químicos", 10000000);

                        db.transaction(
                            function(tx) {
                                tx.executeSql('CREATE TABLE IF NOT EXISTS Produtos(ID INTEGER PRIMARY KEY AUTOINCREMENT, Nome TEXT, PF NUMBER, Exercito NUMBER, Riscos TEXT, PSocorros TEXT, H1 NUMBER, H2 NUMBER, H3 NUMBER, H4 NUMBER)');
                                tx.executeSql('CREATE TABLE IF NOT EXISTS ProdutosOutrosNomes(IDProduto NUMBER, Nome TEXT)');
                                tx.executeSql('CREATE TABLE IF NOT EXISTS Log(ID INTEGER PRIMARY KEY AUTOINCREMENT, Data INTEGER, IP TEXT, Acao TEXT, Detalhes TEXT)');

                                function log(detalhe, acao){
                                    tx.executeSql('INSERT INTO Log (Data, IP, Acao, Detalhes) VALUES(?, ?, ?, ?)', [ Date.now()/1000, "", acao, detalhe ]);
                                }

                                if(obj.Acao==="Cadastrar")
                                {
                                    if(obj.IDProduto)
                                    {
                                        tx.executeSql('DELETE FROM Produtos WHERE ID = '+obj.IDProduto);
                                        tx.executeSql('DELETE FROM ProdutosOutrosNomes WHERE IDProduto = '+obj.IDProduto);
                                        tx.executeSql('INSERT INTO Produtos VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [ obj.IDProduto, obj.Nome, obj.PF, obj.Exercito, obj.Riscos, obj.PSocorros, obj.H1, obj.H2, obj.H3, obj.H4 ]);
                                        for (var i = 0; i < obj.OutrosNomes.length; i++) {
                                            tx.executeSql('INSERT INTO ProdutosOutrosNomes VALUES(?, ?)', [ obj.IDProduto, obj.OutrosNomes[i].nome ]);
                                        }
                                        log(message,"Editar");
                                    }
                                    else
                                    {
                                        var qProd = tx.executeSql('SELECT p.ID FROM Produtos p LEFT OUTER JOIN ProdutosOutrosNomes po ON p.ID = po.IDProduto WHERE p.Nome = \"'+obj.Filtro+'\" or po.Nome = \"'+obj.Filtro+'\" group by p.ID, p.Nome');

                                        if(qProd.rows.length>0)
                                            throw "Produto "+obj.Nome+" já existe.";

                                        tx.executeSql('INSERT INTO Produtos (Nome, PF, Exercito, Riscos, PSocorros, H1, H2, H3, H4) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)', [ obj.Nome, obj.PF, obj.Exercito, obj.Riscos, obj.PSocorros, obj.H1, obj.H2, obj.H3, obj.H4 ]);
                                        for (var i = 0; i < obj.OutrosNomes.length; i++) {
                                            tx.executeSql('INSERT INTO ProdutosOutrosNomes VALUES((SELECT max(p.ID) from Produtos p), ?)', [ obj.OutrosNomes[i].nome ]);
                                        }
                                        log(message,"Cadastrar");
                                    }
                                    webSocket.sendTextMessage("Ok");
                                }
                                else if(obj.Acao==="Listar")
                                {
                                    var qProd = tx.executeSql('SELECT p.ID as IDProduto, p.Nome FROM Produtos p LEFT OUTER JOIN ProdutosOutrosNomes po ON p.ID = po.IDProduto WHERE p.Nome like \"%'+obj.Filtro+'%\" or po.Nome like \"%'+obj.Filtro+'%\" group by p.ID, p.Nome');

                                    var r = [];
                                    for (var i=0; i<qProd.rows.length; i++) {
                                            // Each row is a standard JavaScript array indexed by
                                            // column names.
                                            r.push(qProd.rows.item(i));
                                        }
                                    webSocket.sendTextMessage(JSON.stringify({"lista":r}));
                                }
                                else if(obj.Acao==="Buscar")
                                {
                                    var qProd = tx.executeSql('SELECT p.ID as IDProduto, p.Nome, group_concat(po.Nome) as OutrosNomes, p.PF, p.Exercito, p.Riscos, p.PSocorros, p.H1, p.H2, p.H3, p.H4 FROM Produtos p LEFT OUTER JOIN ProdutosOutrosNomes po ON p.ID = po.IDProduto WHERE p.ID = '+obj.ID+' group by p.ID, p.Nome, p.Riscos, p.PSocorros, p.H1, p.H2, p.H3, p.H4');

                                    webSocket.sendTextMessage(JSON.stringify(qProd.rows.item(0)));
                                }
                                else if(obj.Acao==="Excluir")
                                {
                                    if(!obj.IDProduto)
                                        throw "Produto não encontrado.";

                                    tx.executeSql('DELETE FROM Produtos WHERE ID = '+obj.IDProduto);
                                    tx.executeSql('DELETE FROM ProdutosOutrosNomes WHERE IDProduto = '+obj.IDProduto);
                                    log(message,"Excluir");
                                    webSocket.sendTextMessage("Ok");
                                }
                            }
                        );
                    }
                    catch(err) {
                        webSocket.sendTextMessage(err);
                    }
                })
                messageBox.text = "conectou\n" + messageBox.text;
            }
            onErrorStringChanged: {
                appendMessage(qsTr("Server error: %1").arg(errorString))
            }
            Component.onCompleted: {
                console.log(url)
            }
        }
        }

        Text {
            id: messageBox
            anchors.fill: parent
        }
    }
}
